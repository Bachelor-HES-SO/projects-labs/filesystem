# -*- coding: utf-8 -*-
import hexdump
import stat
import struct
from fs import ext2

class ext2_file_api(object):
    def __init__(self,filesystem):
        self.fs = filesystem
        self.file_table = []
        return

    def find_free_fd(self):
        if len(self.file_table) == 0:
            self.file_table.append(None)
        i = 0
        for f in self.file_table:
            if f == None:
                return i
            i += 1
        self.file_table.append(None)
        return i

    #open a file, i.e reserve a file descriptor
    #in the open file table, pointing to the corresponding
    #inode. file descriptor is just an handle used to find the
    #corresponding inode. This handle is allocated by the filesystem.
    def open(self,path):
        inode = self.fs.namei(path)
        fd_index = self.find_free_fd()
        self.file_table[fd_index] = inode
        return fd_index

    #release file descriptor entry, should we flush buffers : no, this is separate ?
    # openfiles[fd] = None
    def close(self,fd):
        self.file_table[fd] = None
        return

    #read nbytes from the file descriptor previously opened, starting at the given offset
    def read(self,fd,file_offset,count):
        res = ''
        block_size = self.fs.block_size
        inode = self.fs.inodes_list[self.file_table[fd]]
        if file_offset > inode.i_size:
            total_left = 0
        else:
            total_left = min(inode.i_size - file_offset, count)
        # buffer_offset = 0
        while total_left > 0:
            block_num = self.fs.bmap(inode, file_offset // block_size)
            offset_in_block = file_offset % block_size
            to_read = min(block_size - offset_in_block, total_left)
            if block_num != 0:
                blk = self.fs.bloc_device.read_bloc(block_num, 1)
                res += blk[offset_in_block : offset_in_block + to_read]
            file_offset += to_read
            total_left -= to_read
        return res

    #get the attributes of a node in a stat dictionnary :
    # keys st_ctime, st_mtime, st_nlink, st_mode, st_size, st_gid, st_uid, st_atime
    #{'st_ctime': 1419027551.4907832, 'st_mtime': 1419027551.4907832, \
    # 'st_nlink': 36, 'st_mode': 16877, 'st_size': 4096, 'st_gid': 0, \
    #  'st_uid': 0, 'st_atime': 1423220038.6543322}
    def attr(self,path):
        inode_num = self.fs.namei(path)
        inode = self.fs.inodes_list[inode_num]
        res = {
            'st_ctime' : inode.i_ctime,
            'st_mtime' : inode.i_mtime,
            'st_blocks' : inode.i_size / 512 + (1 if inode.i_size % 512 > 0 else 0),
            'st_gid' : inode.i_gid,
            'st_nlink' : inode.i_links_count,
            'st_mode' : inode.i_mode,
            'st_blksize' : self.fs.block_size,
            'st_size' : inode.i_size,
            'st_atime' : inode.i_atime,
            'st_uid' : inode.i_uid
        }
        return res

    # implementation of readdir(3) :
    # open the named file, and read each dir_entry in it
    # note that is not a syscall but a function from the libc
    def dodir(self,path):
        num_dinode = self.fs.namei(path);
        dinode = self.fs.inodes_list[num_dinode]
        dirlist = []
        i = 0
        finish = False
        while i < dinode.i_size and not finish:
            block_num = self.fs.bmap(dinode, i)
            i += 1;
            if block_num == 0:
                continue

            dir_entry = self.fs.bloc_device.get_bloc(block_num)
            dir_entry_offset = 0
            while dir_entry_offset < self.fs.block_size and not finish:
                inode_value = struct.unpack("<I", dir_entry[dir_entry_offset : dir_entry_offset + 4])[0]
                rec_len = struct.unpack("<H", dir_entry[dir_entry_offset + 4 : dir_entry_offset + 4 + 2])[0]
                name_len = struct.unpack("<B", dir_entry[dir_entry_offset + 6 : dir_entry_offset + 6 + 1])[0]
                if name_len > 0:
                    inode_name = struct.unpack("<"+str(name_len)+"s", dir_entry[dir_entry_offset + 8 : dir_entry_offset + 8 + name_len])[0]
                    dirlist.append(inode_name)
                if rec_len > 0:
                    dir_entry_offset += rec_len
                else:
                    finish = True
        return dirlist

    # For all symlink shorter than 60 bytes long, the data is stored within the inode itself;
    # it uses the fields which would normally be used to store the pointers to data blocks.
    # This is a worthwhile optimisation as it we avoid allocating a full block for the symlink,
    # and most symlinks are less than 60 characters long.
    def readlink(self,path):
        num_inode = self.fs.namei(path)
        inode = self.fs.inodes_list[num_inode]
        if (inode.i_mode & 0xa) == 0xa: # si c'est un lien symbolique
            if (inode.i_size > 60):
                fd = self.open(path)
                res = self.read(fd, 0, inode.i_size)
                self.close(fd)
            else:
                st = inode.raw_inode[40:100] # tableau de chaines de caractères qui construisent le path
                res = ""
                i = 0
                while st[i] != '\x00' :
                    res += st[i]
                    i += 1
        else :
            raise OSError(2, 'No such symbolic link')
        return res
