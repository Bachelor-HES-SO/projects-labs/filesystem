## L'outil FUSE
* Pour monter l'image en utilisant ext2fuse.py :
`python ext2fuse.py <fs_image_name> <mount_folder>`

## La récupération des fichiers supprimés
* Pour exécuter l'outil :
`python back_up_files.py <image_file_name>`
* Une liste des fichiers supprimés va apparaître
* Il faut saisir le numéro d'inode à récupérer
* Si tout s'est bien passé, le fichier supprimé va être récupéré dans le répértoire "backups/" sous le nom "recovered_inode" (inode est le numéro d'inode)
