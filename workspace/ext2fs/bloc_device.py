#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
# emulate a simple bloc device using a file
# reading it only by bloc units

class bloc_device(object):
	def __init__(self, blksize, pathname):
		self.image = open(pathname, 'r')
		self.bloc_size = blksize

	def jump_to_bloc(self, bloc_num):
		self.image.seek(self.bloc_size * bloc_num, 0)

	def get_bloc(self, bloc_num):
		self.jump_to_bloc(bloc_num)
		return self.image.read(self.bloc_size)

	def seek_bloc(self, bloc_num, offset, size):
		self.jump_to_bloc(bloc_num)
		self.image.seek(offset, 1)
		return self.image.read(size)

	def read_bloc(self, bloc_num, numofblk = 1):
		self.jump_to_bloc(bloc_num)
		# self.bloc_count = numofblk
		bloc = ''
		for num in xrange(bloc_num,bloc_num + numofblk):
			bloc += self.get_bloc(num)
		return bloc
