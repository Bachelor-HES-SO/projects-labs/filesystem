# -*- coding: utf-8 -*-

from fs_inode import *
from fs_superbloc import *
from fs_bloc_group import *
from bitarray import *
import struct
from bloc_device import *
from hexdump import hexdump

# test = 2
#
# testfile = "smallimg0.ext2.img"
# if test == 2:
#     testfile = "mediumimg0.ext2.img"

BLOCK_SIZE = 1024
FIRST_BGROUP_OFFSET = 2048
BGROUP_SIZE = 32

#This class implements read only ext2 filesystem

class ext2(object):
    def __init__(self,filename):
        ################
        # initialize superbloc
        ################
        self.superbloc = ext2_superbloc(filename)

        ################
        # initialize bloc_device
        ################
        self.block_size = BLOCK_SIZE << self.superbloc.s_log_block_size # taille du bloc
        self.bloc_device = bloc_device(self.block_size, filename)

        ################
        # initialize block group descriptor list
        ################
        bgroup_count = 1 + (self.superbloc.s_blocks_count - 1) / self.superbloc.s_blocks_per_group # nombre de groupe de bloc
        self.bgroup_desc_list = []
        bgroup_bloc_num = FIRST_BGROUP_OFFSET / self.block_size # numéro du bloc dans le groupe de bloc
        for i in range(0, bgroup_count):
            bgroup_bloc_offset = i * BGROUP_SIZE + FIRST_BGROUP_OFFSET * (bgroup_bloc_num == 0) # offset du bloc dans le groupe de bloc en cours
            bgroup = self.bloc_device.seek_bloc(bgroup_bloc_num, bgroup_bloc_offset, BGROUP_SIZE)
            self.bgroup_desc_list.append(ext2_bgroup_desc(bgroup))

        ################
        # initialize inode map from the first group block
        ################
        inode_map = self.bloc_device.get_bloc(self.bgroup_desc_list[0].bg_inode_bitmap) # chargement du bitmap d'inode
        self.inode_map = bitarray(endian='little')
        self.inode_map.frombytes(inode_map) # conversion de la chaine de caractère (hexa) en binaire

        ################
        # initialize block map from the first group block
        ################
        bloc_map = self.bloc_device.get_bloc(self.bgroup_desc_list[0].bg_block_bitmap) # chargement du bitmap de blocs
        self.bloc_map = bitarray(endian='little')
        self.bloc_map.frombytes(bloc_map)

        ################
        # initialize inodes list of the device
        ################
        self.inodes_list = [ext2_inode()] # 1er élément initialisé à des champs de 0
        inodes_per_block = self.block_size / self.superbloc.s_inode_size # nombre d'inode par bloc
        itable_blocks = self.superbloc.s_inodes_per_group / inodes_per_block # nombre de bloc par table d'inode
        for i in range(0, bgroup_count):
            cnt = 1
            inode_table_bloc_num = self.bgroup_desc_list[i].bg_inode_table # offset du premier bloc de la table d'inode
            for j in range(0, itable_blocks):
                for k in range(0, inodes_per_block):
                    inode_table_bloc_offset = k * self.superbloc.s_inode_size # offset dans le bloc en cours de la table d'inode en cours
                    raw_inode = self.bloc_device.seek_bloc(inode_table_bloc_num + j, inode_table_bloc_offset, self.superbloc.s_inode_size)
                    self.inodes_list.append(ext2_inode(raw_inode, cnt))
                    cnt = cnt + 1

        return
    #find the directory inode number of a path
    #given : ex '/usr/bin/cat' return the inode
    #of '/usr/bin'
    def dirnamei(self,path):
        splited_path = path.split('/')
        return namei(splited_path[len(splited_path) - 2])

    #find an inode number according to its path
    #ex : '/usr/bin/cat'
    #only works with absolute paths
    def namei(self, path):
        if path == "/":
            return 2
        splited_path = path.split('/')
        splited_path.pop(0)
        inode_num = 2 # root inode "/"
        for s in splited_path:
            inode_num = self.lookup_entry(self.inodes_list[inode_num], s)
        return inode_num

    def bmap(self,inode,blk):
        # cas 0 dans le cours : blocs adressables directement
        nbr_direct_blocks = 12
        if blk < nbr_direct_blocks:
            return inode.i_blocks[blk]
        # cas 1 dans le cours : blocs adressables avec une seule indirection
        blk -= nbr_direct_blocks
        nbr_indirect_blocks = self.block_size / 4 # on divise par 4 (4*8=32bits par case) pour obtenir le nombre de blocs indirects
        if blk < nbr_indirect_blocks:
            if inode.i_blocks[12] == 0:
                return 0;
            indirect_bloc = self.bloc_device.get_bloc(inode.i_blocks[12]) # 12 c'est l'indice du bloc indirect
            if len(indirect_bloc) == 0 :
                return 0
            return struct.unpack("<I", indirect_bloc[blk * 4 : blk * 4 + 4])[0]

        # cas 2 dans le cours : blocs adressables avec une double indirection
        blk -= nbr_indirect_blocks
        nbr_dbl_indirect_blocks = nbr_indirect_blocks * nbr_indirect_blocks # le nombre de blocs double indirects
        if blk < nbr_dbl_indirect_blocks:
            if inode.i_blocks[13] == 0 :
                return 0
            dbl_indirect_bloc = self.bloc_device.get_bloc(inode.i_blocks[13]) # 13 c'est l'indice du bloc doublement indirect
            if len(dbl_indirect_bloc) == 0 :
                return 0
            double_indirect_bloc_num = struct.unpack("<I", dbl_indirect_bloc[(blk // nbr_indirect_blocks) * 4 : (blk // nbr_indirect_blocks) * 4 + 4])[0]
            if double_indirect_bloc_num == 0: # tester si le bloc n'est pas utilisé
                                              # A value of 0 in this array effectively terminates it with no further block being defined
                return 0
            spl_indirect_bloc = self.bloc_device.get_bloc(double_indirect_bloc_num)
            if len(spl_indirect_bloc) == 0 :
                return 0
            return struct.unpack("<I", spl_indirect_bloc[(blk % nbr_indirect_blocks) * 4 : (blk % nbr_indirect_blocks) * 4 + 4])[0]

        # cas 3 : blocs adressables avec une triple indirection
        blk -= nbr_dbl_indirect_blocks
        nbr_trpl_indirect_blocks = nbr_indirect_blocks * nbr_indirect_blocks * nbr_indirect_blocks # le nombre de blocs triple indirects
        if blk < nbr_trpl_indirect_blocks:
            if inode.i_blocks[14] == 0 :
                return 0
            trpl_indirect_bloc = self.bloc_device.get_bloc(inode.i_blocks[14]) # 14 c'est l'indice du bloc triplement indirect
            if len(trpl_indirect_bloc) == 0 :
                return 0
            triple_indirect_bloc_num = struct.unpack("<I", trpl_indirect_bloc[(blk // nbr_indirect_blocks) * 4 : (blk // nbr_indirect_blocks) * 4 + 4])[0]
            if triple_indirect_bloc_num == 0:
                return 0
            dbl_indirect_bloc = self.bloc_device.get_bloc(triple_indirect_bloc_num)
            double_indirect_bloc_num = struct.unpack("<I", dbl_indirect_bloc[(blk % nbr_indirect_blocks) * 4 : (blk % nbr_indirect_blocks) * 4 + 4])[0]
            if double_indirect_bloc_num == 0:
                return 0
            spl_indirect_bloc = self.bloc_device.get_bloc(double_indirect_bloc_num)
            return struct.unpack("<I", spl_indirect_bloc[(blk % nbr_indirect_blocks) * 4 : (blk % nbr_indirect_blocks) * 4 + 4])[0]
        return 0

    #lookup for a name in a directory, and return its inode number,
    #given inode directory dinode
    #ext2 release 0 store directories in a linked list of records
    #pointing to the next by length
    # - records cannot span multiple blocs.
    # - the end of the linked list as an inode num equal to zero.
    def lookup_entry(self,dinode,name):
        i = 0
        finish = False
        while i < dinode.i_size :
            block_num = self.bmap(dinode, i)
            i += 1;
            if block_num == 0:
                continue
            dir_entry = self.bloc_device.get_bloc(block_num)
            dir_entry_offset = 0
            while dir_entry_offset < self.block_size and not finish:
                inode_value = struct.unpack("<I", dir_entry[dir_entry_offset : dir_entry_offset + 4])[0]
                rec_len = struct.unpack("<H", dir_entry[dir_entry_offset + 4 : dir_entry_offset + 4 + 2])[0]
                name_len = struct.unpack("<B", dir_entry[dir_entry_offset + 6 : dir_entry_offset + 6 + 1])[0]
                if name_len > 0:
                    inode_name = struct.unpack("<"+str(name_len)+"s", dir_entry[dir_entry_offset + 8 : dir_entry_offset + 8 + name_len])[0]
                    if inode_name == name:
                        return inode_value
                if rec_len > 0:
                    dir_entry_offset += rec_len
                else :
                    finish = True
        return 0
