# -*- coding: utf-8 -*-

from fs import ext2
import stat
import sys
import datetime
import time

def read_inode(fs, inode_num):
    res = ""
    for i in xrange(0, fs.inodes_list[inode_num].i_size):
        bmap_bloc = fs.bmap(fs.inodes_list[inode_num], i)
        if bmap_bloc is 0:
            return res
        # print fs.bloc_device.read_bloc(bmap_bloc)
        res += fs.bloc_device.read_bloc(bmap_bloc)
    return res

def insert_inode_list(i_list, fs, inode_num) :
    i = inode_num
    res = read_inode(fs, i)
    if res != "" :
        inode_date = fs.inodes_list[i].i_dtime
        current_date = int(time.time())
        if (inode_date != 0 and inode_date < current_date) :
            i_list.append({\
                'inode' : i, \
                'size' : fs.inodes_list[i].i_size, \
                'uid' : fs.inodes_list[i].i_uid, \
                'deleted at' : datetime.datetime.fromtimestamp(inode_date).strftime('%d-%m-%Y %H:%M:%S')})

def backup_list(fs, without_links):
    deleted_list = []
    i = 1
    for s in fs.inode_map:
        if not s:
            if without_links :
                if fs.inodes_list[i].i_links_count == 0 and stat.S_ISREG(fs.inodes_list[i].i_mode) and not stat.S_ISDIR(fs.inodes_list[i].i_mode) :
                    insert_inode_list(deleted_list, fs, i)
            else :
                if stat.S_ISREG(fs.inodes_list[i].i_mode) and not stat.S_ISDIR(fs.inodes_list[i].i_mode):
                    insert_inode_list(deleted_list, fs, i)
        i += 1
    return deleted_list

def display_list(deleted_list) :
    print len(deleted_list), "deleted files found"
    print "inode\t\t\tsize\t\t\tuid\t\t\tdeleted on"
    for l in deleted_list :
        print l['inode'], "\t\t\t", l['size'], "\t\t\t", l['uid'], "\t\t\t", l['deleted at']

def recover(i_list, i, fs) :
    for el in i_list :
        if el['inode'] == i :
            res = read_inode(fs, i)
            new_file = open("backups/recovered" + str(i), "w") # Argh j'ai tout écrasé !
            new_file.write(res)
            new_file.close()
            print "File with inode number", i, "was recovered with success under backup/recovered" + str(i)
            return True
    print "Inode number does not exist! Please try again"
    return False

################
##    main    ##
################
if len(sys.argv) != 2 :
    print "use :"
    print "\tpython back_up_files.py <image_file_name>"
    exit(0)
testfile = sys.argv[1]
f = ext2(testfile)
while True :
    print "Choose an option :"
    print "\t1. Display deleted files with no hard links"
    print "\t2. Display deleted files with existing hard links"
    choice = input()
    if choice == 1 or choice == 2 :
        if choice == 1 :
            recup_list = backup_list(f, True)
        if choice == 2 :
            recup_list = backup_list(f, False)
        display_list(recup_list)
        if len(recup_list) != 0 :
            inode_num = input("Please give the inode number of the file to be recovered : ")
            while not recover(recup_list, inode_num, f) :
                inode_num = input("Please give the inode number of the file to be recovered : ")
        break
exit(0)
