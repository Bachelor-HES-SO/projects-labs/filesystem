#!/usr/bin/python2.7
# -*-coding:Utf-8 -*

import bitarray
import hexdump
import struct

class FileSystem(file): # FileSystem extends file
	def jumpToBloc(self,num):
		return self.seek(1024 * num, 0)

	def getBloc(self, num):
		self.jumpToBloc(num)
		return file.read(1024)

	def printBloc(self):
		print(':'.join(byte.encode('hex') for byte in file.read(1024)))

file = FileSystem("minixfs_lab1.img")
rawdata = [file.getBloc(bloc) for bloc in xrange(4341,4348)] \
+ [file.getBloc(bloc) for bloc in xrange(4349,4861)] \
+ [file.getBloc(bloc) for bloc in xrange(4863,4988)]
rawdata = ''.join(rawdata)
archive = open('linux-0.95.tgz', 'w+')
archive.write(rawdata)
